package co.runloop.bletest;

import android.bluetooth.BluetoothDevice;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.recyclerview.widget.RecyclerView;

public class DevicesAdapter extends RecyclerView.Adapter<DevicesAdapter.DeviceViewHolder> {

    public interface OnDeviceClickListener {
        void onDeviceClick(BluetoothDevice device);
    }

    private static final String TAG = DevicesAdapter.class.getSimpleName();

    private List<BluetoothDevice> devices;
    private OnDeviceClickListener deviceClickListener;

    public DevicesAdapter() {
        devices = new ArrayList<>();
    }

    public void addDevice(BluetoothDevice device) {
        for (BluetoothDevice d : devices) {
            if (d.getAddress().equals(device.getAddress())) {
                return;
            }
        }
        devices.add(device);
        notifyItemInserted(devices.size() - 1);
    }

    public void setDeviceClickListener(OnDeviceClickListener deviceClickListener) {
        this.deviceClickListener = deviceClickListener;
    }

    public void setDevices(List<BluetoothDevice> devices) {
        this.devices = devices;
        notifyDataSetChanged();
    }

    public void clearDevices() {
        int count = devices.size();
        devices.clear();
        notifyItemRangeRemoved(0, count);
    }

    @NonNull
    @Override
    public DeviceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_device, parent, false);
        return new DeviceViewHolder(root);
    }

    @Override
    public void onBindViewHolder(@NonNull DeviceViewHolder holder, int position) {
        holder.bind(devices.get(position));
    }

    @Override
    public int getItemCount() {
        return devices.size();
    }

    @Override
    public void onViewRecycled(@NonNull DeviceViewHolder holder) {
        super.onViewRecycled(holder);
        holder.clear();
    }

    class DeviceViewHolder extends RecyclerView.ViewHolder {

        private BluetoothDevice device;

        private TextView titleTv;
        private TextView addressTv;

        public DeviceViewHolder(@NonNull View itemView) {
            super(itemView);
            titleTv = itemView.findViewById(R.id.item_device_title);
            addressTv = itemView.findViewById(R.id.item_device_address_tv);
            itemView.setOnClickListener(view -> {
                if (deviceClickListener != null) {
                    deviceClickListener.onDeviceClick(device);
                }
            });
        }

        public void bind(BluetoothDevice device) {
            this.device = device;
            titleTv.setText(device.getName());
            addressTv.setText(device.getAddress());
            Log.i(TAG, "name: " + device.getName()
                    + " address: " + device.getAddress()
                    + " bondState: " + device.getBondState()
                    + " type: " + device.getType()
                    + " uuids count: " + (device.getUuids() == null ? "null" : device.getUuids().length));
        }

        public void clear() {
            device = null;
            titleTv.setText(null);
            addressTv.setText(null);
        }
    }
}
