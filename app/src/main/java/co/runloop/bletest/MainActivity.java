package co.runloop.bletest;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final int LOCATION_PERMS_RC = 98;
    private static final int BLUETOOTH_ENABLE_RC = 99;

    private static final int SCAN_DURATION_MS = 20000;

    private static final UUID VEHICLE_STATE_SERVICE_UUID = UUID.fromString("eea340ac-2bb9-4bea-9b5f-9d905d1cc42f");
    private static final UUID VEHICLE_STATE_CHARACTERISTIC_UUID = UUID.fromString("43b4df89-8807-4edb-b785-6e97fdc0a769");

    private static final UUID AUTH_SERVICE_UUID = UUID.fromString("59023e68-62ca-4eb0-b36f-c4def6bc1ce9");
    private static final UUID AUTH_STATE_CHARACTERISTIC_UUID = UUID.fromString("acea06c5-9139-4a4d-9723-3c548ee41bbf");
    private static final UUID AUTH_PASSWORD_CHARACTERISTIC_UUID = UUID.fromString("310b524f-3737-4a33-9a62-d5e5b55a6786");

    private ToggleButton scanBtn;
    private Button disconnectBtn;
    private Button bondBtn;
    private NestedScrollView scrollView;
    private TextView logTv;
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private Button authBtn;
    private ToggleButton vehicleStateBtn;
    private EditText passwordEt;
    private Button requestVehicleStateBtn;

    private Handler handler;
    private DevicesAdapter adapter;
    private BluetoothManager bluetoothManager;
    private BluetoothAdapter bluetoothAdapter;
    private BluetoothGatt gatt;
    private BluetoothDevice device;
    private List<BluetoothGattService> gattServices;
    private BroadcastReceiver bluetoothReceiver;
    private BroadcastReceiver bluetoothBondReceiver;
    private StringBuilder logBuilder;
    private boolean scaning;

    private void logInfo(String msg) {
        handler.post(() -> {
            logBuilder.append("INFO: " + msg + "\n");
            logTv.setText(logBuilder.toString());
            scrollView.fullScroll(View.FOCUS_DOWN);
            Log.i(TAG, msg);
        });
    }

    private void logError(String msg) {
        handler.post(() -> {
            logBuilder.append("ERROR: " + msg + "\n");
            logTv.setText(logBuilder.toString());
            scrollView.fullScroll(View.FOCUS_DOWN);
            Log.e(TAG, msg);
        });
    }

    private void logDebug(String msg) {
        handler.post(() -> {
            logBuilder.append("DEBUG: " + msg + "\n");
            logTv.setText(logBuilder.toString());
            scrollView.fullScroll(View.FOCUS_DOWN);
            scrollView.scrollTo(0, logTv.getHeight());
            Log.d(TAG, msg);
        });
    }

    private void authVehicleService() {
        BluetoothGattService service = gatt.getService(AUTH_SERVICE_UUID);
        if (service != null) {
            logInfo("try auth");
            BluetoothGattCharacteristic c = service.getCharacteristic(AUTH_PASSWORD_CHARACTERISTIC_UUID);
            String password = passwordEt.getText().toString();
            logInfo("password: " + password);
            logInfo("password is set to characteristic: " + c.setValue(password));
            logInfo("write characteristic: " + gatt.writeCharacteristic(c));
        } else {
            logError("Auth service not found");
        }
    }

    private void changeVehicleState(boolean isChecked) {
        BluetoothGattService service = gatt.getService(VEHICLE_STATE_SERVICE_UUID);
        if (service != null) {
            logInfo("try change vehicle state");
            BluetoothGattCharacteristic c = service.getCharacteristic(VEHICLE_STATE_CHARACTERISTIC_UUID);
            c.setValue(isChecked ? 1 : 0, BluetoothGattCharacteristic.FORMAT_UINT8, 0);
            gatt.writeCharacteristic(c);
        } else {
            logError("VehicleState service not found");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        authBtn = findViewById(R.id.auth_btn);
        authBtn.setOnClickListener(view -> authVehicleService());

        scrollView = findViewById(R.id.log_scroll);
        logTv = findViewById(R.id.log_tv);

        vehicleStateBtn = findViewById(R.id.vehicle_state_btn);
        vehicleStateBtn.setOnCheckedChangeListener((buttonView, isChecked) -> changeVehicleState(isChecked));

        scanBtn = findViewById(R.id.scan_btn);
        scanBtn.setOnCheckedChangeListener((btnView, isChecked) -> {
            if (isChecked) {
                scanBluetoothDevices();
            } else {
                stopScanBluetoothDevices();
            }
        });

        disconnectBtn = findViewById(R.id.disconnect_btn);
        disconnectBtn.setOnClickListener(view -> disconnectBluetoothDevice());

        bondBtn = findViewById(R.id.bond_btn);
        bondBtn.setOnClickListener(view -> bondBluetoothDevice());

        requestVehicleStateBtn = findViewById(R.id.vehicle_status_request_btn);
        requestVehicleStateBtn.setOnClickListener(view -> requestVehicleState());

        passwordEt = findViewById(R.id.password_et);
        progressBar = findViewById(R.id.progressbar);

        initRecyclerView();
        registerBluetoothStateReceiver();
        registerBluetoothBondReceiver();

        logBuilder = new StringBuilder();
        gattServices = new ArrayList<>();
        handler = new Handler();

        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, "BLE not supported!", Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        if (!locationPermissionsIsAvailable()) {
            scanBtn.setEnabled(false);
            ActivityCompat.requestPermissions(this,
                    new String[]{
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION},
                    LOCATION_PERMS_RC);
        }

        initBluetooth();
        if (!bluetoothIsEnabled()) {
            setEnableForAllButtons(false);
            requestBluetooth();
        } else {
            updateActionButtons();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        disconnectBluetoothDevice();
        unregisterReceiver(bluetoothReceiver);
        unregisterReceiver(bluetoothBondReceiver);
    }

    private void initBluetooth() {
        bluetoothManager = (BluetoothManager) getSystemService(BLUETOOTH_SERVICE);
        bluetoothAdapter = bluetoothManager.getAdapter();
    }

    private void bondBluetoothDevice() {
        if (device.getBondState() == BluetoothDevice.BOND_NONE) {
            logInfo("create bond request success: " + device.createBond());
        }
    }

    private void registerBluetoothStateReceiver() {
        bluetoothReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.STATE_OFF);
                logInfo("Bluetooth state changed, state: " + state);
                updateActionButtons();
                if (state != BluetoothAdapter.STATE_ON) {
                    scanBtn.setChecked(false);
                }
            }
        };
        IntentFilter bluetoothFilter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
        registerReceiver(bluetoothReceiver, bluetoothFilter);
    }

    private void registerBluetoothBondReceiver() {
        bluetoothBondReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int state = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, BluetoothDevice.BOND_NONE);
                logInfo("Bluetooth bond state changed, state: " + state);
            }
        };
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        registerReceiver(bluetoothBondReceiver, filter);
    }

    private void requestVehicleState() {
        BluetoothGattService service = gatt.getService(VEHICLE_STATE_SERVICE_UUID);
        if (service != null) {
            BluetoothGattCharacteristic characteristic = service.getCharacteristic(VEHICLE_STATE_CHARACTERISTIC_UUID);
            gatt.readCharacteristic(characteristic);
        } else {
            logError("vehicle service is null!");
        }
    }

    private void initRecyclerView() {
        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        DividerItemDecoration divider = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(divider);
        adapter = new DevicesAdapter();
        adapter.setDeviceClickListener(device -> {
            adapter.clearDevices();
            scanBtn.setChecked(false);
            this.device = device;
            logDebug("selected device: " + device);
            disconnectBluetoothDevice();
            gatt = device.connectGatt(this, false, new BluetoothGattCallback() {
                @Override
                public void onPhyUpdate(BluetoothGatt gatt, int txPhy, int rxPhy, int status) {
                    logDebug("onPhyUpdate");
                }

                @Override
                public void onPhyRead(BluetoothGatt gatt, int txPhy, int rxPhy, int status) {
                    logDebug("onPhyRead");
                }

                @Override
                public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
                    logDebug("onConnectionStateChange, status: " + status + " newState: " + newState);
                    logDebug("bondState: " + device.getBondState());
                    switch (newState) {
                        case BluetoothProfile.STATE_CONNECTED:
                            logDebug("Device connected!");
                            logDebug("connected, discover services started: " + gatt.discoverServices());
                            break;
                        case BluetoothProfile.STATE_CONNECTING:
                            logDebug("Device connecting...");
                            break;
                        case BluetoothProfile.STATE_DISCONNECTED:
                            logDebug("Device disconnected!");
                            break;
                        case BluetoothProfile.STATE_DISCONNECTING:
                            logDebug("Device disconnecting...");
                            break;
                    }
                    if (newState != BluetoothProfile.STATE_CONNECTED) {
                        handler.post(() -> updateActionButtons());
                    }
                }

                @Override
                public void onServicesDiscovered(BluetoothGatt gatt, int status) {
                    logDebug("onServiceDiscovered, status: " + status);
                    List<BluetoothGattService> services = gatt.getServices();
                    logDebug("services count: " + services.size());

                    for (BluetoothGattService service : services) {
                        logDebug("service, uuid: " + service.getUuid() + " type: " + service.getType());

                        for (BluetoothGattCharacteristic characteristic : service.getCharacteristics()) {
                            gatt.setCharacteristicNotification(characteristic, true);
                            logDebug(" read characteristic, uuid: " + characteristic.getUuid()
                                    + " val: " + characteristic.getStringValue(0)
                                    + " perms: " + characteristic.getPermissions()
                                    + " properties: " + characteristic.getProperties()
                                    + " writeType: " + characteristic.getWriteType());

                            for (BluetoothGattDescriptor descriptor : characteristic.getDescriptors()) {
                                logDebug("    read descriptor, uuid: " + descriptor.getUuid()
                                        + " bytes value to string: " + (descriptor.getValue() == null ? "null" : new String(descriptor.getValue()))
                                        + " perms: " + descriptor.getPermissions());
                            }
                        }
                    }
                    gattServices.addAll(services);
                }

                @Override
                public void onCharacteristicRead(BluetoothGatt gatt,
                                                 BluetoothGattCharacteristic characteristic,
                                                 int status) {
                    logDebug("onCharacteristicRead, status: " + status
                            + " characteristic: " + characteristic.getStringValue(0));
                    if (characteristic.getUuid().equals(VEHICLE_STATE_CHARACTERISTIC_UUID)) {
                        int state = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
                        logInfo("vehicle state: " + state);
                        handler.post(() -> {
                            Toast.makeText(MainActivity.this, "vehicle state: " + state, Toast.LENGTH_SHORT).show();
                        });
                    }
                }

                @Override
                public void onCharacteristicWrite(BluetoothGatt gatt,
                                                  BluetoothGattCharacteristic characteristic,
                                                  int status) {
                    logDebug("onCharacteristicWrite, status: " + status
                            + " characteristic: " + characteristic.getStringValue(0));
                }

                @Override
                public void onCharacteristicChanged(BluetoothGatt gatt,
                                                    BluetoothGattCharacteristic characteristic) {
                    int flag = characteristic.getProperties();
                    int format = -1;
                    if ((flag & 0x01) != 0) {
                        format = BluetoothGattCharacteristic.FORMAT_UINT16;
                        logDebug("Heart rate format UINT16.");
                    } else {
                        format = BluetoothGattCharacteristic.FORMAT_UINT8;
                        logDebug("Heart rate format UINT8.");
                    }
                    logDebug("onCharacteristicChanged characteristic: " + characteristic.getUuid()
                            + " asHex: " + parseHexValue(characteristic.getValue())
                            + " asString: " + characteristic.getStringValue(0)
                            + " asInt: " + characteristic.getIntValue(format, 0));
                }

                @Override
                public void onDescriptorRead(BluetoothGatt gatt,
                                             BluetoothGattDescriptor descriptor,
                                             int status) {
                    logDebug("onDescriptorRead, status: " + status
                            + " descriptor: " + descriptor.getUuid());
                }

                @Override
                public void onDescriptorWrite(BluetoothGatt gatt,
                                              BluetoothGattDescriptor descriptor,
                                              int status) {
                    logDebug("onDescriptorWrite");
                }

                @Override
                public void onReliableWriteCompleted(BluetoothGatt gatt, int status) {
                    logDebug("onReliableWriteCompleted");
                }

                @Override
                public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
                    logDebug("onReadRemoteRssi");
                }

                @Override
                public void onMtuChanged(BluetoothGatt gatt, int mtu, int status) {
                    logDebug("onMtuChanged");
                }
            });
            updateActionButtons();
            adapter.clearDevices();
        });
        recyclerView.setAdapter(adapter);
    }

    private String parseHexValue(byte[] val) {
        if (val != null && val.length > 0) {
            StringBuilder builder = new StringBuilder(val.length);
            for (byte b : val) {
                builder.append(String.format("%02X", b));
            }
            logDebug("val from hex: " + builder.toString());
            return builder.toString();
        }
        return null;
    }

    private boolean locationPermissionsIsAvailable() {
        int r1 = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        int r2 = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        return r1 == PackageManager.PERMISSION_GRANTED && r2 == PackageManager.PERMISSION_GRANTED;
    }


    private void disconnectBluetoothDevice() {
        if (gatt != null) {
            gatt.close();
            gatt = null;
            updateActionButtons();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == BLUETOOTH_ENABLE_RC) {
            if (resultCode == RESULT_OK) {
                updateActionButtons();
            } else if (resultCode == RESULT_CANCELED) {
                logError("bluetooth enable request was denied!");
                Toast.makeText(this, "Bluetooth is required!", Toast.LENGTH_LONG).show();
                setEnableForAllButtons(false);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMS_RC:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    updateActionButtons();
                } else {
                    Toast.makeText(this, "Location permissions is required!", Toast.LENGTH_LONG).show();
                    setEnableForAllButtons(false);
                }
                break;
        }
    }

    private boolean bluetoothIsEnabled() {
        return bluetoothAdapter != null && bluetoothAdapter.isEnabled();
    }

    private void requestBluetooth() {
        Intent bleIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(bleIntent, BLUETOOTH_ENABLE_RC);
    }


    private boolean ignoreScanResult;
    private BluetoothLeScanner bluetoothLeScanner;
    private final ScanCallback scanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            handler.post(() -> {
                if (!ignoreScanResult) {
                    if (result.getDevice() != null) {
                        adapter.addDevice(result.getDevice());
                        recyclerView.smoothScrollToPosition(adapter.getItemCount() - 1);
                    } else {
                        logError("device from result is null!");
                    }
                }
            });
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
        }

        @Override
        public void onScanFailed(int errorCode) {
            handler.post(() -> {
                scaning = false;
                updateActionButtons();
                hideProgress();
                logError("onScanFailed, errorCode: " + errorCode);
            });
        }
    };
    private final Runnable scanRunnable = () -> scanBtn.setChecked(false);


    private void scanBluetoothDevices() {
        ignoreScanResult = false;
        adapter.clearDevices();

        showProgress();

        if (bluetoothLeScanner == null) {
            bluetoothLeScanner = bluetoothAdapter.getBluetoothLeScanner();
        }

        scaning = true;
        bluetoothLeScanner.startScan(scanCallback);
        handler.postDelayed(scanRunnable, SCAN_DURATION_MS);
    }

    private void stopScanBluetoothDevices() {
        handler.removeCallbacksAndMessages(scanRunnable);
        scaning = false;
        updateActionButtons();
        bluetoothLeScanner.stopScan(scanCallback);
        hideProgress();
    }

    private void setEnableForAllButtons(boolean enable) {
        scanBtn.setEnabled(enable);
        disconnectBtn.setEnabled(enable);
        vehicleStateBtn.setEnabled(enable);
        authBtn.setEnabled(enable);
        bondBtn.setEnabled(enable);
        requestVehicleStateBtn.setEnabled(enable);
    }

    private void updateActionButtons() {
        scanBtn.setEnabled(bluetoothAdapter.isEnabled() && !scaning);
        boolean enabled = gatt != null && bluetoothAdapter.isEnabled();
        disconnectBtn.setEnabled(enabled);
        authBtn.setEnabled(enabled);
        vehicleStateBtn.setEnabled(enabled);
        bondBtn.setEnabled(enabled);
        requestVehicleStateBtn.setEnabled(enabled);
    }

    private void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    private void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }
}
